<?php
require_once "animal.php";
class Ape extends Animal
{
    protected $legs = 4;
    function set_legs($legs)
    {
        $this->legs = $legs;
    }
    function get_legs()
    {
        return $this->legs;
    }

    public function yell()
    {
        echo "Auooo";
    }
}
