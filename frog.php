<?php
require_once "animal.php";
class Frog extends Animal
{
    protected $legs = 4;

    public function jump()
    {
        echo "Hop Hop";
    }
    public function get_legs()
    {
        return $this->legs;
    }
    public function set_legs($legs)
    {
        $this->legs = $legs;
    }
}
