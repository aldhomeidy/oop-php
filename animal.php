<?php
class Animal
{
    private $legs = 2;
    private $cold_blooded = "false";
    public function __construct($name)
    {
        $this->name = $name;
    }

    function set_legs($legs)
    {
        $this->legs = $legs;
    }
    function get_legs()
    {
        return $this->legs;
    }
    function set_cold_boold($blood)
    {
        $this->cold_blooded = $blood;
    }
    function get_cold_blood()
    {
        return $this->cold_blooded;
    }
}
