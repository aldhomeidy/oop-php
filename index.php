<?php
require_once "animal.php";
require_once "frog.php";
require_once "ape.php";


$animal = new Animal("Shaun");
$kodok = new Frog("Katak");
$sungokong = new Ape("Kera");

echo "<h2>Jawaban Soal Release 0</h2>";
echo $animal->name . "<br>";
echo $animal->get_legs() . "<br>";
echo $animal->get_cold_blood() . "<br>";

echo "<h2>Jawaban Soal Release 1</h2>";
$kodok->jump();
echo "<br>";
$sungokong->yell();
